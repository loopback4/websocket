"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebsocketApplication = void 0;
const rest_1 = require("@loopback/rest");
const server_1 = require("./server");
class WebsocketApplication extends rest_1.RestApplication {
    constructor(options = {}) {
        super(options);
        this.wsServer = new server_1.WebSocketServer(this, options === null || options === void 0 ? void 0 : options.socketio);
    }
    websocketRoute(controllerClass, namespace) {
        return this.wsServer.route(controllerClass, namespace);
    }
    async start() {
        var _a;
        await super.start();
        const httpServer = (_a = this.restServer.httpServer) === null || _a === void 0 ? void 0 : _a.server;
        await this.wsServer.start(httpServer);
    }
    async stop() {
        await this.wsServer.stop();
        await super.stop();
    }
}
exports.WebsocketApplication = WebsocketApplication;
//# sourceMappingURL=application.js.map