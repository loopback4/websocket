/// <reference types="node" />
import { Constructor, Context } from '@loopback/context';
import { Server, ServerOptions, Socket } from 'socket.io';
import { WebSocketMetadata } from "./decorator";
import http from 'http';
import https from 'https';
export declare type SockIOMiddleware = (socket: Socket, fn: (err?: any) => void) => void;
/**
 * A websocket server
 */
export declare class WebSocketServer extends Context {
    ctx: Context;
    private options?;
    private io;
    constructor(ctx: Context, options?: ServerOptions | undefined);
    /**
     * Register a sock.io middleware function
     * @param fn
     */
    use(fn: SockIOMiddleware): Server<import("socket.io/dist/typed-events").DefaultEventsMap, import("socket.io/dist/typed-events").DefaultEventsMap, import("socket.io/dist/typed-events").DefaultEventsMap, any>;
    /**
     * Register a websocket controller
     * @param ControllerClass
     * @param meta
     */
    route(controllerClass: Constructor<{
        [method: string]: Function;
    }>, meta?: WebSocketMetadata | string | RegExp): Server<import("socket.io/dist/typed-events").DefaultEventsMap, import("socket.io/dist/typed-events").DefaultEventsMap, import("socket.io/dist/typed-events").DefaultEventsMap, any> | import("socket.io").Namespace<import("socket.io/dist/typed-events").DefaultEventsMap, import("socket.io/dist/typed-events").DefaultEventsMap, import("socket.io/dist/typed-events").DefaultEventsMap, any>;
    /**
     * Start the websocket server
     */
    start(httpServer: http.Server | https.Server): Promise<void>;
    /**
     * Stop the websocket server
     */
    stop(): Promise<void>;
}
