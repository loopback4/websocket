"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebSocketServer = void 0;
const context_1 = require("@loopback/context");
const socket_io_1 = require("socket.io");
const controller_factory_1 = require("./controller-factory");
const decorator_1 = require("./decorator");
const debug = require('debug')('loopback:websocket');
/**
 * A websocket server
 */
class WebSocketServer extends context_1.Context {
    constructor(ctx, options) {
        super(ctx);
        this.ctx = ctx;
        this.options = options;
        this.io = new socket_io_1.Server(options);
        ctx.bind('ws.server').to(this.io);
    }
    /**
     * Register a sock.io middleware function
     * @param fn
     */
    use(fn) {
        return this.io.use(fn);
    }
    /**
     * Register a websocket controller
     * @param ControllerClass
     * @param meta
     */
    route(controllerClass, meta) {
        if (meta instanceof RegExp || typeof meta === 'string') {
            meta = { namespace: meta };
        }
        if (meta == null) {
            meta = (0, decorator_1.getWebSocketMetadata)(controllerClass);
        }
        const nsp = (meta === null || meta === void 0 ? void 0 : meta.namespace) ? this.io.of(meta.namespace) : this.io;
        if (meta === null || meta === void 0 ? void 0 : meta.name) {
            this.ctx.bind(`ws.namespace.${meta.name}`).to(nsp);
        }
        /* eslint-disable @typescript-eslint/no-misused-promises */
        // @ts-ignore
        nsp.on('connection', async (socket) => {
            console.log('connection', 'connection');
            debug('Websocket connected: id=%s namespace=%s', socket.id, socket.nsp.name);
            // Create a request context
            const reqCtx = new context_1.Context(this);
            // Bind websocket
            reqCtx.bind('ws.socket').to(socket);
            // Instantiate the controller instance
            await new controller_factory_1.WebSocketControllerFactory(reqCtx, controllerClass).create(socket);
        });
        return nsp;
    }
    /**
     * Start the websocket server
     */
    async start(httpServer) {
        this.io.attach(httpServer, this.options);
    }
    /**
     * Stop the websocket server
     */
    async stop() {
        const close = new Promise((resolve, reject) => {
            this.io.close(() => {
                resolve();
            });
        });
        await close;
    }
}
exports.WebSocketServer = WebSocketServer;
//# sourceMappingURL=server.js.map