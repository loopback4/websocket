import { ApplicationConfig } from '@loopback/core';
import { RestApplication } from '@loopback/rest';
import { WebSocketServer } from "./server";
import { Constructor } from "@loopback/context";
import { Namespace } from "socket.io";
export { ApplicationConfig };
export declare class WebsocketApplication extends RestApplication {
    readonly wsServer: WebSocketServer;
    constructor(options?: ApplicationConfig);
    websocketRoute(controllerClass: Constructor<{
        [method: string]: Function;
    }>, namespace?: string | RegExp): Namespace;
    start(): Promise<void>;
    stop(): Promise<void>;
}
